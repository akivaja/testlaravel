<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::all(); 
        $id=Auth::id();
        return view('tests.index', ['tests'=>$tests]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
            if (Gate::denies('manager')) {
                abort(403,"Sorry you are not allowed to create products..");
            } 
           

        return view ('tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = new Test();
        $id=Auth::id();
        $test->name = $request->name;
        $test->price = $request->price;
        $test->user_id = $id;
        $test->save();
        return redirect('tests');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to edit products..");
        } 
        $test = Test::find($id);
        return view('tests.edit', compact('test'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       

        $test = Test::find($id);
        if(!$test->user->id == Auth::id()) return(redirect('tests'));
        $test -> update($request->except(['_token']));
        
        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        return redirect('tests');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete products..");
        } 
        $test = Test::find($id);
        $test->delete();
        return redirect('tests');

    }

    public function done($id)
    {
        $test = Test::find($id);
        $test->status = 1;
        $test->save();
        $test->update();
        return redirect('tests');

    }




}

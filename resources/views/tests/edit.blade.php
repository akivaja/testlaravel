@extends('layouts.app')
@section('content')

<h1>Edit Product</h1>
<form method = 'post' action="{{action('TestController@update', $test->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "name">update name::</label>
    <input type= "text" class = "form-control" name= "name" value = "{{$test->name}}">
</div>

<div class = "form-group">
    <label for = "price">Update price:</label>
    <input type= "number" class = "form-control" name= "price" value = "{{$test->price}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="save changes">
</div>

</form>


@endsection

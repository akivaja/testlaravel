@extends('layouts.app')
@section('content')


<h1>Create New Product</h1>
<form method = 'post' action="{{action('TestController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "name">Enter name:</label>
    <input type= "text" class = "form-control" name= "name">
</div>

<div class = "form-group">
    <label for = "price">Enter price:</label>
    <input type= "number" class = "form-control" name= "price">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Create">
</div>

</form>

@endsection

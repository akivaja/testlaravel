<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('tests', 'TestController')->middleware('auth');

Route::get('/delete/{id}', 'TestController@destroy')->name('delete');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/done/{id}', 'TestController@done')->name('done');

